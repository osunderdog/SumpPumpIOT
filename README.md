# SumpPumpIOT

Dependencies


Adafruit ADSS1x15

git clone https://github.com/adafruit/Adafruit_Python_ADS1x15.git

cd Adafruit_Python_ADS1x15

sudo python setup.py install


AWS Device SDK for Python

git clone https://github.com/aws/aws-iot-device-sdk-python.git

cd aws-iot-device-sdk-python

sudo python setup.py install

