# run pub/sub sample app using certificates downloaded in package

CREDDIR=./credentials

printf "\nRuning pub/sub sample application...\n"
python SumpPumpSub.py \
       -e a2p1et2u5m77w1.iot.us-east-1.amazonaws.com \
       -r "$CREDDIR/root-CA.crt" \
       -c "$CREDDIR/SumpPumpDetector_01.cert.pem" \
       -k "$CREDDIR/SumpPumpDetector_01.private.key"
